# Тестовое задание для infotecs "Программист на языке Python"
### ТЗ:

>Реализовать HTTP-сервер для предоставления информации по географическим объектам.
>Данные взять из географической базы данных GeoNames.


### Реализация
Версия Python 3.8.2.

Для реализации web интерфейса используется Flask.

Инструкцию по запуску скрипта можно найти в [документации фреймворка](https://flask.palletsprojects.com/en/1.1.x/quickstart/).

Интерфейс будет работать по адресу 127.0.0.1 и порту 8000.

База данных импортируется в кодировке 'utf-8', после чего загружается в список для дальнейшей обработки.

### Задание 1:

Метод доступен по адресу 

```web-idl
127.0.0.1:8000/geonameid/<geonameid>
```

Данный метод принимает 1 параметр GET: 'geonameid', который должен содержать id города в базе данных geoname

Поиск искомого города происходит через бинарный поиск. 

После обнаружения города информация о нём переносится в словарь, который имеет структуры JSON-файла (В дальнейшем для отображения информации о городе будет использоваться эта структура):

```json
'geonameid': id,
'name': name,
'asciiname': asciiname, 
'alternatenames': alternatenames,
'latitude': latitude, 
'longtitude': longtitude,
'feature_class': feature_class, 
'feature_code': feature_code,
'county_code': country_code, 
'cc2': cc2,
'admin1_code': admin1_code, 
'admin2_code': admin2_code,
'admin3_code': admin3_code, 
'admin4_code': admin4_code,
'population': population, 
'elevation': elevation,
'dem': dem, 
'timezone': timezone,
'modification_date': modification_date
```



### Задание 2:

Метод доступен по адресу 

```web-idl
127.0.0.1:8000/citiesPage/<p>/<n>
```

Данный метод принимает 2 параметра GET: 'p', который должен содержать номер страницы, и 'n', который должен содержать количество городов на странице.

Для вывода списка городов, по заданным параметрам, сначала составляется список, удовлетворяющий данные параметры:

```python
cities_page_list = cities_list[((page_number - 1) * cities_page_count):(page_number * cities_page_count)]
```

После чего, с использованием цикла for, информация о каждом городе форматируется в JSON формат.



### Задание 3 и Доп. задание 1:

Метод доступен по адресу 

```web-idl
127.0.0.1:8000/compareCities/<city_1>/<city_2>
```

Данный метод принимает 2 параметра GET: 'city_1' и 'city_2', которые должны содержать названия двух городов (на русском языке).

Для поиска города по названию на русском языке необходимо разделить параметр, содержащий альтернативные название города, по знаку ",", после чего искать вхождение искомого названия в этом списке:

```python
find_result = [i for i, value in enumerate(cities_list) if city_name_1 in value[3].split(',')]
```

Для нахождения более северного города сравниваются показатели широты, город с большей широтой располагается севернее.

Для нахождения разницы во времени использовались библиотеки timezone, datetime. После чего вычисляется модуль разницы двух временных зон и на сколько отличаются часовые пояса. 

### Доп. задание 2:

Метод доступен по адресу

```web-idl
127.0.0.1:8000/findStart/<namestart>
```

Данный метод принимает один парамент GET: 'namestart', который должен содержать начало названия населенного пункта (на русском языке).

Для поиска города по названию на русском языке необходимо разделить параметр, содержащий альтернативные название города, по знаку ",", после чего в каждом названии из скписка искать совпадение начала с введенным текстом.

При нахождение совпадения, добавляем в список для вывода. Так проходим по всем населенным пунктам из базы данных.

Затем формируется JSON для отображения возможных названий.

Таким образом для нахождения возможных продолжений названия используется 2 цикла, один - для перебора всех населенных пунктов, другой - для проверки альтернативных названий населенного пункта.
