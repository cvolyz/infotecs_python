import json
from math import fabs
from datetime import datetime as dt
from pytz import timezone
from flask import Flask, render_template, request, jsonify

app = Flask(__name__)

file = open('RU.txt', 'r', encoding='utf-8')
'''
    Перенос базы данных из файла в список
'''

cities_list = []
for line in file:
    cities_list.append(line.split('\t'))


@app.route('/geonameid/<geonameid>')
def cityInfo(geonameid):
    cities_count = 201177
    low = 0
    high = cities_count - 1
    while low <= high:
        mid = (low + high) // 2
        if int(geonameid) < int(cities_list[mid][0]):
            high = mid - 1
        elif int(geonameid) > int(cities_list[mid][0]):
            low = mid + 1
        else:
            city_dict = {'geonameid': cities_list[mid][0], 'name': cities_list[mid][1],
                         'asciiname': cities_list[mid][2], 'alternatenames': cities_list[mid][3],
                         'latitude': cities_list[mid][4], 'longtitude': cities_list[mid][5],
                         'feature_class': cities_list[mid][6], 'feature_code': cities_list[mid][7],
                         'county_code': cities_list[mid][8], 'cc2': cities_list[mid][9],
                         'admin1_code': cities_list[mid][10], 'admin2_code': cities_list[mid][11],
                         'admin3_code': cities_list[mid][12], 'admin4_code': cities_list[mid][13],
                         'population': cities_list[mid][14], 'elevation': cities_list[mid][15],
                         'dem': cities_list[mid][16], 'timezone': cities_list[mid][17],
                         'modification_date': cities_list[mid][18][:-1]}
            return json.dumps(city_dict, ensure_ascii=False)
    else:
        return 'Нет номера'


@app.route('/citiesPage/<p>/<n>')
def citiesPage(p, n):
    page_number = int(p)
    cities_page_count = int(n)
    cities_page_list = cities_list[((page_number - 1) * cities_page_count):(page_number * cities_page_count)]
    for i in range(len(cities_page_list)):
        cities_page_list[i] = {'geonameid': cities_page_list[i][0], 'name': cities_page_list[i][1],
                               'asciiname': cities_page_list[i][2], 'alternatenames': cities_page_list[i][3],
                               'latitude': cities_page_list[i][4], 'longtitude': cities_page_list[i][5],
                               'feature_class': cities_page_list[i][6], 'feature_code': cities_page_list[i][7],
                               'county_code': cities_page_list[i][8], 'cc2': cities_page_list[i][9],
                               'admin1_code': cities_page_list[i][10], 'admin2_code': cities_page_list[i][11],
                               'admin3_code': cities_page_list[i][12], 'admin4_code': cities_page_list[i][13],
                               'population': cities_page_list[i][14], 'elevation': cities_page_list[i][15],
                               'dem': cities_page_list[i][16], 'timezone': cities_page_list[i][17],
                               'modification_date': cities_page_list[i][18][:-1]}
    return json.dumps(cities_page_list, ensure_ascii=False)

@app.route('/compareCities/<city_1>/<city_2>')
def compareCities(city_1, city_2):
    city_name_1 = city_1
    city_name_2 = city_2
    find_result = [i for i, value in enumerate(cities_list) if city_name_1 in value[3].split(',')]

    max_population = -1
    for i in find_result:
        if max_population < int(cities_list[int(i)][14]):
            max_population = int(cities_list[int(i)][14])
            city_1 = {'geonameid': cities_list[int(i)][0], 'name': cities_list[int(i)][1],
                      'asciiname': cities_list[int(i)][2], 'alternatenames': cities_list[int(i)][3],
                      'latitude': cities_list[int(i)][4], 'longtitude': cities_list[int(i)][5],
                      'feature_class': cities_list[int(i)][6], 'feature_code': cities_list[int(i)][7],
                      'county_code': cities_list[int(i)][8], 'cc2': cities_list[int(i)][9],
                      'admin1_code': cities_list[int(i)][10], 'admin2_code': cities_list[int(i)][11],
                      'admin3_code': cities_list[int(i)][12], 'admin4_code': cities_list[int(i)][13],
                      'population': cities_list[int(i)][14], 'elevation': cities_list[int(i)][15],
                      'dem': cities_list[int(i)][16], 'timezone': cities_list[int(i)][17],
                      'modification_date': cities_list[int(i)][18][:-1]}
    find_result = [i for i, value in enumerate(cities_list) if city_name_2 in value[3].split(',')]
    max_population = -1
    for i in find_result:
        if max_population < int(cities_list[int(i)][14]):
            max_population = int(cities_list[int(i)][14])
            city_2 = {'geonameid': cities_list[int(i)][0], 'name': cities_list[int(i)][1],
                      'asciiname': cities_list[int(i)][2], 'alternatenames': cities_list[int(i)][3],
                      'latitude': cities_list[int(i)][4], 'longtitude': cities_list[int(i)][5],
                      'feature_class': cities_list[int(i)][6], 'feature_code': cities_list[int(i)][7],
                      'county_code': cities_list[int(i)][8], 'cc2': cities_list[int(i)][9],
                      'admin1_code': cities_list[int(i)][10], 'admin2_code': cities_list[int(i)][11],
                      'admin3_code': cities_list[int(i)][12], 'admin4_code': cities_list[int(i)][13],
                      'population': cities_list[int(i)][14], 'elevation': cities_list[int(i)][15],
                      'dem': cities_list[int(i)][16], 'timezone': cities_list[int(i)][17],
                      'modification_date': cities_list[int(i)][18][:-1]}


    c_1 = dt.now(timezone(city_1['timezone'])).strftime('%z')[:3]
    c_2 = dt.now(timezone(city_2['timezone'])).strftime('%z')[:3]
    print( int(c_1) - int(c_2))
    rtrn = [city_1, city_2, {'Разница в часовых поясах': int(fabs(int(c_1) - int(c_2)))}]
    if city_1['latitude'] > city_2['latitude']:
        if c_1 == c_2:
            rtrn[2].update({'Севернее город': city_name_1, 'Города в одном часовом поясе': True})
            return json.dumps(rtrn, ensure_ascii=False)
        else:
            rtrn[2].update({'Севернее город': city_name_1, 'Города в одном часовом поясе': False})
            return json.dumps(rtrn, ensure_ascii=False)
    else:
        if c_1 == c_2:
            rtrn[2].update({'Севернее город': city_name_2, 'Города в одном часовом поясе': True})
            return json.dumps(rtrn, ensure_ascii=False)
        else:
            rtrn[2].update({'Севернее город': city_name_2, 'Города в одном часовом поясе': False})
            return json.dumps(rtrn, ensure_ascii=False)

@app.route('/findStart/<namestart>')
def findStart (namestart):
    find_result = []
    for i in cities_list:
        name_city_sp = i[3].split(',')
        for j in name_city_sp:
            if j.startswith(namestart):
                find_result.append(j)
                break
    print(find_result)
    if find_result:
        return json.dumps(find_result, ensure_ascii=False)
    else:
        return 'Города не найдены'
if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8000)
